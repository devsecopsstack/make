## [1.4.1](https://gitlab.com/to-be-continuous/make/compare/1.4.0...1.4.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([162a4ff](https://gitlab.com/to-be-continuous/make/commit/162a4ffa17a75e97395ac6c759660ba4e602c391))

# [1.4.0](https://gitlab.com/to-be-continuous/make/compare/1.3.0...1.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([9abd31f](https://gitlab.com/to-be-continuous/make/commit/9abd31f13ee39a35efa48db714370abe74eafbec))

# [1.3.0](https://gitlab.com/to-be-continuous/make/compare/1.2.0...1.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([f8b5ef9](https://gitlab.com/to-be-continuous/make/commit/f8b5ef9f6ecb6e25a52007908f26f7f8be3c7cb0))

# [1.2.0](https://gitlab.com/to-be-continuous/make/compare/1.1.3...1.2.0) (2023-11-10)


### Features

* introduce MAKE_PROJECT_DIR ([eeb3bc8](https://gitlab.com/to-be-continuous/make/commit/eeb3bc8863c889c6ac351e2d31b70cf71961f1cf))

## [1.1.3](https://gitlab.com/to-be-continuous/make/compare/1.1.2...1.1.3) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([ad571d1](https://gitlab.com/to-be-continuous/make/commit/ad571d188132195b19ff70f191b9bae432cb7756))

## [1.1.2](https://gitlab.com/to-be-continuous/make/compare/1.1.1...1.1.2) (2023-08-01)


### Bug Fixes

* switch to alpinelinux/build-base ([2b6d497](https://gitlab.com/to-be-continuous/make/commit/2b6d49748803054854f083f95ea8730368a1e461))

## [1.1.1](https://gitlab.com/to-be-continuous/make/compare/1.1.0...1.1.1) (2023-08-01)


### Bug Fixes

* failure while decoding a secret [@url](https://gitlab.com/url)@ does not cause the job to fail (warning message) ([1111e7b](https://gitlab.com/to-be-continuous/make/commit/1111e7bca125edacdf37ba521049567d70be70d2))

# [1.1.0](https://gitlab.com/to-be-continuous/make/compare/1.0.0...1.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([4e9318a](https://gitlab.com/to-be-continuous/make/commit/4e9318ad5bde645f1d8efde6fbdeba9adea6d20b))

# 1.0.0 (2023-02-14)


### Features

* initial release ([9c52195](https://gitlab.com/to-be-continuous/make/commit/9c52195b82278d5a4b0072f30ec6e7813624c073))
